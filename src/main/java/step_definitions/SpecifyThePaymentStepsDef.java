package step_definitions;

import Tools.SelenideTools;
import io.cucumber.java.ru.И;
import org.openqa.selenium.By;

public class SpecifyThePaymentStepsDef {
    SelenideTools selTools = new SelenideTools();

    private static final By ONLINE_PAYMENT = By.xpath("//label[@for='payment_method_2']");

    private static final By NEXT_BUTTON = By.xpath("//input[@value='Далее']");

    @И("^указываем способ оплаты$")
    public void specifyThePaymentMethod() {
        selTools.clickButton(ONLINE_PAYMENT);
        selTools.clickButton(NEXT_BUTTON);
    }
}
