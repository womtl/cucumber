package step_definitions;

import Tools.SelenideTools;
import io.cucumber.java.ru.И;
import org.openqa.selenium.By;

public class ConfirmTheOrderPageStepsDef {
    SelenideTools selTools = new SelenideTools();

    private static final By ADDITIONAL_INFO = By.xpath("//textarea[@name='order_add_info']");

    private static final By CONFIRM_CHECKBOX = By.xpath("//input[@type='checkbox']");

    private static final By CONFIRM_ORDER_BUTTON = By.xpath("//input[@name='finish_registration']");

    @И("^подтверждаем заказ$")
    public void confirmTheOrder() {
        selTools.sendKeysButton(ADDITIONAL_INFO,"Пришлите, пожалуйста, сообщение на почту, когда заказ будет доставлен.");
        selTools.clickButton(CONFIRM_CHECKBOX);
        selTools.clickButton(CONFIRM_ORDER_BUTTON);
    }
}
