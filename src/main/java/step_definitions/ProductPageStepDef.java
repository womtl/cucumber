package step_definitions;

import Tools.SelenideTools;
import io.cucumber.java.ru.И;
import org.openqa.selenium.By;

public class ProductPageStepDef {
    SelenideTools selTools = new SelenideTools();

    private static final By WISHLIST = By.xpath("//input[@class='btn button btn-wishlist']");

    @И("^добавляем товар в список желаемого$")
    public void addProductWishlist() {
        selTools.clickButton(WISHLIST);
    }
}
