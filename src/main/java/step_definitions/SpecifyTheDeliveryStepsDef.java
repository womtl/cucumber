package step_definitions;

import Tools.SelenideTools;
import io.cucumber.java.ru.И;
import org.openqa.selenium.By;

public class SpecifyTheDeliveryStepsDef {
    SelenideTools selTools = new SelenideTools();

    private static final By EXPRESS_DELIVERY = By.xpath("//label[@for='shipping_method_2']");

    private static final By NEXT_BUTTON = By.xpath("//input[@value='Далее']");

    @И("^указываем способ доставки")
    public void specifyTheDeliveryMethod() {
        selTools.clickButton(EXPRESS_DELIVERY);
        selTools.clickButton(NEXT_BUTTON);
    }
}
