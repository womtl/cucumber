package step_definitions;

import Tools.SelenideTools;
import io.cucumber.java.ru.И;
import org.openqa.selenium.By;

public class BasketPageStepsDef {
    SelenideTools selTools = new SelenideTools();

    private static final By UPDATE_BASKET = By.xpath("//img[@onclick='document.updateCart.submit();']");

    private static final By CHECKOUT = By.xpath("//a[@class='btn btn-arrow-right']");

    @И("^обновляем корзину$")
    public void updateBasket() {
        selTools.clickButton(UPDATE_BASKET);
    }

    @И("^нажимаем на кнопку 'оформить заказ'$")
    public void checkout() {
        selTools.clickButton(CHECKOUT);
    }
}
