package step_definitions;

import Data.RandomPhoneNumber;
import Tools.SelenideTools;
import io.cucumber.java.ru.И;
import org.openqa.selenium.By;

public class OrderFormStepsDef {
    SelenideTools selTools = new SelenideTools();

    private static final By NAME_FIELD = By.xpath("//input[@name='f_name']");

    private static final By LAST_NAME = By.xpath("//input[@name='l_name']");

    private static final By EMAIL = By.xpath("//input[@id='email']");

    private static final By STREET = By.xpath("//input[@name='street']");

    private static final By POST_CODE = By.xpath("//input[@id='zip']");

    private static final By CITY = By.xpath("//input[@name='city']");

    private static final By REGION = By.xpath("//input[@name='state']");

    private static final By PHONE = By.xpath("//input[@name='phone']");

    private static final By NEXT_BUTTON = By.xpath("//input[@value='Далее']");

    public String randNumber = RandomPhoneNumber.tsifri();
    @И("^заполняем форму заказа$")
    public void fillOutTheOrderForm() {
        selTools.sendKeysButton(NAME_FIELD,"Тест");
        selTools.sendKeysButton(LAST_NAME,"Тестов");
        selTools.sendKeysButton(EMAIL,"test@test.ru");
        selTools.sendKeysButton(STREET,"Крымский Вал, 10");
        selTools.sendKeysButton(POST_CODE,"119017");
        selTools.sendKeysButton(CITY,"Москва");
        selTools.sendKeysButton(REGION,"Москва");
        selTools.sendKeysButton(PHONE,randNumber);
        selTools.clickButton(NEXT_BUTTON);
    }
}
