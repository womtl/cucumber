package step_definitions;

import Tools.SelenideTools;
import io.cucumber.java.ru.И;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.open;
public class MainPageStepsDef {
        private static final By SHOPPING_BUTTON = By.xpath("/html/body/div/div[2]/div[1]/div/div/nav/div[2]/ul/li[3]");

        SelenideTools selTools = new SelenideTools();

        @И("^открываем страницу$")
        public void goToHome() {
            open("https://qahacking.guru/");
        }

        @И("^переходим в магазин$")
        public void goToShop() {
            selTools.clickButton(SHOPPING_BUTTON);
            selTools.clickButton(SHOPPING_BUTTON);
        }
}

