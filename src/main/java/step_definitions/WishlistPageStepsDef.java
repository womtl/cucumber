package step_definitions;

import Tools.SelenideTools;
import com.codeborne.selenide.Condition;
import io.cucumber.java.ru.И;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class WishlistPageStepsDef {
    SelenideTools selTools = new SelenideTools();

    private static final By PRODUCT_NAME = By.xpath("//div[contains(text(), 'Наименование')]/following-sibling::div[@class='data']/a");

    private static final By BASKET = By.xpath("(//a[@href='/index.php/magazin/izbrannoe/remove_to_cart?number_id=0'])[1]");

    @И("^проверяем название продукта в списке желаемого \"(.*)\"$")
    public void checkProductName(String productName) {
        $(PRODUCT_NAME).shouldHave(Condition.text(productName));
    }

    @И("^добавляем товар в корзину$")
    public void addBasket() {
        selTools.clickButton(BASKET);
    }
}
