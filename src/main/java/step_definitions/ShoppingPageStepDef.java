package step_definitions;

import Tools.SelenideTools;
import io.cucumber.java.ru.И;
import org.openqa.selenium.By;

public class ShoppingPageStepDef {
    private static final By PRODUCTS_NAME = By.xpath("(//a[@href='/index.php/magazin/podkast'])[2]");

    private static final By COUNT = By.xpath("//input[@name='quantity']");

    SelenideTools selTools = new SelenideTools();

    @И("^меняем количество товара на три$")
    public void changeCount() {
        selTools.sendKeysButton(COUNT, "3");
    }

    @И("^выбираем продукт \"(.*)\"$")
    public void selectProduct(String productName) {
        selTools.clickButton(PRODUCTS_NAME);
    }
}
